<?php

function ajout_petiteAnnonce() {
$args = array(
'label' => __('PetitesAnnonces'),
'singular_label' => __('PetitesAnnonces'),
'public' => true,
'show_ui' => true,
'_builtin' => false, // It's a custom post type, not built in
'_edit_link' => 'post.php?post=%d',
'capability_type' => 'post',
'hierarchical' => false,
'rewrite' => array("slug" => "projects"),
'query_var' => "projets", // This goes to the WP_Query schema
'supports' => array('title', 'editor', 'thumbnail') //titre + zone de texte + champs personnalisés + miniature valeur possible : 'title','editor','author','thumbnail','excerpt'
);
register_post_type( 'projet' , $args ); // enregistrement de l'entité projet basé sur les arguments ci-dessus
register_taxonomy_for_object_type('post_tag', 'projet','show_tagcloud=1&hierarchical=true'); // ajout des mots clés pour notre custom post type
//add_action("admin_init", "admin_init"); function pour ajouter des champs personnalisés
//add_action('save_post', 'save_custom'); function pour la sauvegarde de nos champs personnalisés
}
add_action('init', 'ajout_petiteAnnonce');

?>