<!--
/**
Template Name: Page annonce
*/
-->
<?php get_header(); ?>

    <div id="content" class="clearfix">
        
        <div id="main" class="col620 clearfix" role="main">
        		<?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args=array(
						//'caller_get_posts'=>1,
						'post_type' => 'petiteannonce',
						'paged'=>$paged
					);
					$wp_query = new WP_Query($args);?>

				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					
					<?php get_template_part( 'content', 'annonce' ); ?>

				<?php endwhile; // end of the loop. ?>

        </div> <!-- end #main -->

        <?php get_sidebar(); // sidebar 1 ?>

    </div> <!-- end #content -->
        
<?php get_footer(); ?>