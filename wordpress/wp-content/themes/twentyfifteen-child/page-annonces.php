<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/*
Template Name: Page des petites annonces
*/

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php

		  $temp_query = $wp_query;
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$args=array(
			//'caller_get_posts'=>1,
			'post_type' => 'annonce',
			'paged'=>$paged
		);
		$wp_query = new WP_Query($args);
	while ($wp_query->have_posts()) : $wp_query->the_post();


		// Start the loop.
		//while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'content', 'annonces' );

echo "<p>Prix: <?php echo get_post_meta( $post->ID, 'prix_annonce', true ); ?></p>";

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;


		// End the loop.
		endwhile;

		$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID));

		?>




		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>


