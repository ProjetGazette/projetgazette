<?php 

function projet_annonces() {
$args = array(
'label' => __('Annonces'),
'singular_label' => __('Annonce'),
'public' => true,
'show_ui' => true,
'_builtin' => false,// It's a custom post type, not built in
'_edit_link' => 'post.php?post=%d',
'capability_type' => 'post',
'hierarchical' => false,
'rewrite' => array("slug" => "news"),
'query_var' => "annonces",// This goes to the WP_Query schema
'supports' => array('title', 'editor', 'thumbnail')//titre + zone de texte + champs personnalisés + miniature valeur possible : 'title','editor','author','thumbnail','excerpt'
);
register_post_type( 'annonce' , $args );// enregistrement de l'entité projet basé sur les arguments ci-dessus
register_taxonomy_for_object_type('post_tag', 'annonce','show_tagcloud=1&hierarchical=true'); // ajout des mots clés pour notre custom post type
add_action("admin_init", "admin_init");
add_action('save_post', 'save_custom');
}
add_action('init', 'projet_annonces');

?>


<?php

function admin_init(){//initialisation des champs spécifiques
add_meta_box("prix_annonce", "Prix de l'annonce", "prix_annonce", "annonce", "normal", "low");//il s'agit de notre champ personnalisé qui apelera la fonction url_projet()
}
function prix_annonce(){//La fonction qui affiche notre champs personnalisé dans l'administration
global $post;
$custom = get_post_custom($post->ID);//fonction pour récupérer la valeur de notre champ
$prix_annonce = $custom["prix_annonce"][0];
?>
<input size="70" type="text" value="<?php echo $prix_annonce;?>" name="prix_annonce"/>
<?php
}
function save_custom(){//sauvegarde des champs spécifiques
global $post;
if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {// fonction pour éviter  le vidage des champs personnalisés lors de la sauvegarde automatique
return $postID;
}
update_post_meta($post->ID, "prix_annonce", $_POST["prix_annonce"]);//enregistrement dans la base de données
}

?>