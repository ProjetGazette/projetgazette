**** Projet Gazette�Haut de Gironde ****

Ceci est une modification.

Notre groupe est compos� de 4 membres�:
- L�a DESCHAMPS
- Antoine COMBES
- Arthur LANNELUCQ
- Alexis VIGNERON

L�objectif du projet est de cr�er un site pour la gazette "�Haut de Gironde�". 

Depuis ce site, la gazette est cens�e pouvoir�:
- Poster de nouveaux articles.
- G�rer les commentaires (ils devront �tre accept�s par un administrateur avant d��tre affich�s sur les articles).
- Disposer d�un espace r�serv� a la pub, afin de financer la gazette.

Le site devra �galement contenir d�autres pages, comme une page indiquant les mentions l�gales, une page "�A propos�", ainsi qu�une page dans laquelle nous mettrons a disposition un formulaire de contact.

Nous devrons �galement utiliser Google Analytics.

INGESUP � Bachelor 1 - Promo 2014/2015



